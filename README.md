# Matrix Synapse Scripts

Scripts to help with running a matrix-synapse homeserver.

# 1. matrix-shutdown-room.sh 

### A simple script to shutdown/remove a room from a homeserver.

This script takes 4 pieces of user input and executes them with curl from the command line.
Information needed for the script:
1. Matrix server URI, e.g. matrix.example.com
2. Admin user name, e.g. @admin:example.com
3. Admin user access token
4. Internal room ID, e.g. !aBcdEFghiklmnOPqr:example.com

To find the user access token will vary depending on the client used.
For Element Client, go to All Settings|Help & About|Access Token, then click to reveal.

To run, login to your homeserver and execute the following: 

`bash -i <(curl -s https://gitlab.com/Goose-Tech/matrix-synapse-scripts/-/raw/master/matrix-shutdown-room.sh)`

For official documentation, please visit: https://github.com/matrix-org/synapse/blob/master/docs/admin_api/shutdown_room.md
or https://matrix.org/docs/guides/moderation#removing-users-rooms-and-content