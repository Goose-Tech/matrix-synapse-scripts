#!/bin/bash

echo 'Shutdown/Remove room from homeserver'
echo '------------------------------------'
read -p 'Enter your homeserver URI, e.g. matrix.example.com, then [ENTER]: ' SERVERURI
read -p 'Enter admin user id, e.g. @admin:example.com, then [ENTER]: ' USERID
read -p 'Enter admin user access token (in Element - All Settings|Help & About|Access Token), then [ENTER]: ' ACCESSTOKEN
read -p 'Enter the full internal room ID, e.g. !aBcdEFghiklmnOPqr:example.com, then [ENTER]: ' ROOMID

curl 'https://'"$SERVERURI"'/_synapse/admin/v1/shutdown_room/'"$ROOMID"'' \
-H "Authorization: Bearer "$ACCESSTOKEN"" -XPOST \
-d '{"new_room_user_id": "'"$USERID"'", "message": "Room has been removed from the homeserver.", "room_name": "Room unavailable"}'